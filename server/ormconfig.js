const path = require('path');

module.exports = {
  database: process.env.DATABASE,
  entities: [path.resolve(__dirname, 'dist/entity/*.js')],
  host: 'localhost',
  password: process.env.DATABASE_PASSWORD,
  port: process.env.DATABASE_PORT,
  synchronize: true,
  type: 'postgres',
  username: process.env.DATABASE_USER,
  url: process.env.DATABASE_URL,
};
