import { IsNotEmpty } from 'class-validator';
import { Entity, ManyToOne, OneToMany } from 'typeorm';

import CompletedPractice from './completedPractice';
import Content from './content';
import Employee from './employee';
import Lesson from './lesson';
import Task from './task';

@Entity('practices')
class Practice extends Content {
  @IsNotEmpty()
  @ManyToOne(
    () => Lesson,
    lesson => lesson.practices,
    {
      nullable: false,
    },
  )
  lesson!: Lesson;

  @IsNotEmpty()
  @ManyToOne(
    () => Employee,
    employee => employee.practices,
    {
      nullable: false,
    },
  )
  creator!: Employee;

  @IsNotEmpty()
  @OneToMany(
    () => Task,
    task => task.practice,
    {
      nullable: false,
    },
  )
  tasks!: Task[];

  @OneToMany(
    () => CompletedPractice,
    completedPractice => completedPractice.practice,
  )
  completedPractices?: CompletedPractice[];
}

export default Practice;
