import { IsNotEmpty } from 'class-validator';
import { Entity, ManyToOne, OneToMany } from 'typeorm';

import AnswerFinalTest from './answerFinalTest';
import CompletedQuestionFinalTest from './completedQuestionFinalTest';
import Employee from './employee';
import Lesson from './lesson';
import Question from './question';

@Entity('questions_final_test')
class QuestionFinalTest extends Question {
  @IsNotEmpty()
  @ManyToOne(
    () => Lesson,
    lesson => lesson.questionsFinalTest,
    {
      nullable: false,
    },
  )
  lesson!: Lesson;

  @IsNotEmpty()
  @ManyToOne(
    () => Employee,
    employee => employee.questionsFinalTest,
    {
      nullable: false,
    },
  )
  creator!: Employee;

  @IsNotEmpty()
  @OneToMany(
    () => AnswerFinalTest,
    answerFinalTest => answerFinalTest,
    {
      nullable: false,
    },
  )
  answersFinalTest!: AnswerFinalTest[];

  @OneToMany(
    () => CompletedQuestionFinalTest,
    completedQuestionFinalTest => completedQuestionFinalTest.questionFinalTest,
  )
  completedQuestionsFinalTest?: CompletedQuestionFinalTest[];
}

export default QuestionFinalTest;
