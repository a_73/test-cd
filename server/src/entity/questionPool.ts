import { IsNotEmpty } from 'class-validator';
import { Entity, ManyToOne, OneToMany } from 'typeorm';

import AnswerPool from './answerPool';
import CompletedQuestionPool from './completedQuestionPool';
import Employee from './employee';
import Lesson from './lesson';
import Question from './question';

@Entity('questions_pool')
class QuestionPool extends Question {
  @IsNotEmpty()
  @ManyToOne(
    () => Lesson,
    lesson => lesson.questionsPool,
    {
      nullable: false,
    },
  )
  lesson!: Lesson;

  @IsNotEmpty()
  @ManyToOne(
    () => Employee,
    employee => employee.questionsPool,
    {
      nullable: false,
    },
  )
  creator!: Employee;

  @IsNotEmpty()
  @OneToMany(
    () => AnswerPool,
    answerPool => answerPool.questionPool,
    {
      nullable: false,
    },
  )
  answersPool!: AnswerPool[];

  @OneToMany(
    () => CompletedQuestionPool,
    completedQuestionPool => completedQuestionPool.questionPool,
  )
  completedQuestionsPool?: CompletedQuestionPool[];
}

export default QuestionPool;
