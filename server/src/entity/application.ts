import { IsNotEmpty } from 'class-validator';
import {
  BaseEntity,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import Employee from './employee';
import Student from './student';

@Entity('applications')
class Application extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @IsNotEmpty()
  @ManyToOne(
    () => Employee,
    employee => employee.applications,
    {
      nullable: false,
    },
  )
  employee!: Employee;

  @IsNotEmpty()
  @OneToOne(
    () => Student,
    student => student.application,
    {
      nullable: false,
    },
  )
  @JoinColumn()
  student!: Student;
}

export default Application;
