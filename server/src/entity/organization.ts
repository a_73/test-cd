import { IsString } from 'class-validator';
import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import Employee from './employee';

@Entity('organizations')
class Organization extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @IsString()
  @Column()
  name!: string;

  @IsString()
  @Column()
  city!: string;

  @IsString()
  @Column()
  address!: string;

  @OneToMany(
    () => Employee,
    employee => employee.organization,
  )
  employees?: Employee[];
}

export default Organization;
