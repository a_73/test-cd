import { BaseEntity, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import Lesson from './lesson';
import Practice from './practice';
import Student from './student';

@Entity('completed_practices')
class CompletedPractice extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToOne(
    () => Student,
    student => student.completedPractices,
    {
      nullable: false,
    },
  )
  student!: Student;

  @ManyToOne(
    () => Lesson,
    lesson => lesson.completedPractices,
    {
      nullable: false,
    },
  )
  lesson!: Lesson;

  @ManyToOne(
    () => Practice,
    practice => practice.completedPractices,
    {
      nullable: false,
    },
  )
  practice!: Practice;
}

export default CompletedPractice;
