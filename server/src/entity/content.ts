import { IsString } from 'class-validator';
import { BaseEntity, Column, PrimaryGeneratedColumn } from 'typeorm';

abstract class Content extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @IsString()
  @Column()
  text!: string;
}

export default Content;
