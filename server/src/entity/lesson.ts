import { IsInt, IsNotEmpty, IsString } from 'class-validator';
import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import CompletedPractice from './completedPractice';
import CompletedQuestionFinalTest from './completedQuestionFinalTest';
import CompletedQuestionPool from './completedQuestionPool';
import Employee from './employee';
import Practice from './practice';
import QuestionFinalTest from './questionFinalTest';
import QuestionPool from './questionPool';

@Entity('lessons')
class Lesson extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @IsInt()
  @Column({
    unique: true,
  })
  number!: number;

  @IsString()
  @Column()
  title!: string;

  @IsString()
  @Column()
  description!: string;

  @IsNotEmpty()
  @ManyToOne(
    () => Employee,
    employee => employee.lessons,
    {
      nullable: false,
    },
  )
  creator!: Employee;

  @IsNotEmpty()
  @OneToMany(
    () => QuestionPool,
    questionPool => questionPool.lesson,
    {
      nullable: false,
    },
  )
  questionsPool!: QuestionPool[];

  @OneToMany(
    () => Practice,
    practice => practice.lesson,
  )
  practices!: Practice[];

  @OneToMany(
    () => QuestionFinalTest,
    questionFinalTest => questionFinalTest.lesson,
  )
  questionsFinalTest!: QuestionFinalTest[];

  @OneToMany(
    () => CompletedQuestionPool,
    completedQuestionPool => completedQuestionPool.lesson,
  )
  completedQuestionsPool?: CompletedQuestionPool[];

  @OneToMany(
    () => CompletedPractice,
    completedPractice => completedPractice.lesson,
  )
  completedPractices?: CompletedPractice[];

  @OneToMany(
    () => CompletedQuestionFinalTest,
    completedQuestionFinalTest => completedQuestionFinalTest.lesson,
  )
  completedQuestionsFinalTest?: CompletedQuestionFinalTest[];
}

export default Lesson;
