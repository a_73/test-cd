import { IsBoolean, IsNotEmpty } from 'class-validator';
import { Entity, ManyToOne, OneToMany, Column } from 'typeorm';

import CompletedQuestionFinalTest from './completedQuestionFinalTest';
import Content from './content';
import Employee from './employee';
import QuestionFinalTest from './questionFinalTest';

@Entity('answers_final_test')
class AnswerFinalTest extends Content {
  @IsBoolean()
  @Column()
  isTrue!: boolean;

  @IsNotEmpty()
  @ManyToOne(
    () => QuestionFinalTest,
    questionFinalTest => questionFinalTest.answersFinalTest,
    {
      nullable: false,
    },
  )
  questionFinalTest!: QuestionFinalTest;

  @IsNotEmpty()
  @ManyToOne(
    () => Employee,
    employee => employee.answersFinalTest,
    {
      nullable: false,
    },
  )
  creator!: Employee;

  @OneToMany(
    () => CompletedQuestionFinalTest,
    completedQuestionFinalTest => completedQuestionFinalTest.answerFinalTest,
  )
  completedQuestionsFinalTest?: CompletedQuestionFinalTest[];
}

export default AnswerFinalTest;
