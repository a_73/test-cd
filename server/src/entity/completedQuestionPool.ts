import { IsNotEmpty } from 'class-validator';
import { BaseEntity, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import AnswerPool from './answerPool';
import Lesson from './lesson';
import QuestionPool from './questionPool';
import Student from './student';

@Entity('completed_questions_pool')
class CompletedQuestionPool extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @IsNotEmpty()
  @ManyToOne(
    () => Student,
    student => student.completedQuestionsPool,
    {
      nullable: false,
    },
  )
  student!: Student;

  @IsNotEmpty()
  @ManyToOne(
    () => Lesson,
    lesson => lesson.completedQuestionsPool,
    {
      nullable: false,
    },
  )
  lesson!: Lesson;

  @IsNotEmpty()
  @ManyToOne(
    () => QuestionPool,
    questionPool => questionPool.completedQuestionsPool,
    {
      nullable: false,
    },
  )
  questionPool!: QuestionPool;

  @IsNotEmpty()
  @ManyToOne(
    () => AnswerPool,
    answerPool => answerPool.completedQuestionsPool,
    {
      nullable: false,
    },
  )
  answerPool!: AnswerPool;
}

export default CompletedQuestionPool;
