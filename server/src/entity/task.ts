import { IsInt, IsNotEmpty } from 'class-validator';
import { Column, Entity, ManyToOne } from 'typeorm';

import Content from './content';
import Employee from './employee';
import Practice from './practice';

@Entity('tasks')
class Task extends Content {
  @IsInt()
  @Column()
  number!: number;

  @IsInt()
  @Column()
  priority!: number;

  @Column()
  actionType!: string;

  @Column('simple-array')
  objects!: string[];

  @IsNotEmpty()
  @ManyToOne(
    () => Practice,
    practice => practice.tasks,
    {
      nullable: false,
    },
  )
  practice!: Practice;

  @IsNotEmpty()
  @ManyToOne(
    () => Employee,
    employee => employee.tasks,
    {
      nullable: false,
    },
  )
  creator!: Employee;
}

export default Task;
