import { IsInt, IsString, IsUrl } from 'class-validator';
import { Column } from 'typeorm';

import Content from './content';

abstract class Question extends Content {
  @IsInt()
  @Column()
  number!: number;

  @IsUrl()
  @Column({
    nullable: true,
  })
  picture?: string;

  @IsString()
  @Column()
  explanation!: string;
}

export default Question;
