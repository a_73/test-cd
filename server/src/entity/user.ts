import { IsAlpha, IsEmail, IsUrl } from 'class-validator';
import { BaseEntity, Column, PrimaryGeneratedColumn } from 'typeorm';

abstract class User extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @IsEmail()
  @Column({
    unique: true,
  })
  email!: string;

  @IsAlpha()
  @Column()
  lastName!: string;

  @IsAlpha()
  @Column()
  firstName!: string;

  @IsUrl()
  @Column({
    default: '../',
  })
  picture!: string;
}

export default User;
