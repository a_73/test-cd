import { IsNotEmpty, IsString } from 'class-validator';
import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import Employee from './employee';
import Student from './student';

@Entity('groups_students')
class GroupStudents extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @IsString()
  @Column()
  name!: string;

  @IsString()
  @Column()
  description!: string;

  @IsNotEmpty()
  @ManyToOne(
    () => Employee,
    employee => employee.groupsStudents,
    {
      nullable: false,
    },
  )
  employee!: Employee;

  @OneToMany(
    () => Student,
    student => student.groupStudents,
  )
  students?: Student[];
}

export default GroupStudents;
