import { Entity, Column, ManyToOne, OneToMany, OneToOne } from 'typeorm';

import Application from './application';
import GroupStudents from './groupStudents';
import CompletedPractice from './completedPractice';
import CompletedQuestionPool from './completedQuestionPool';
import CompletedQuestionFinalTest from './completedQuestionFinalTest';
import User from './user';

@Entity('students')
class Student extends User {
  @Column({
    nullable: true,
    unique: true,
  })
  userIdApple?: string;

  @Column({
    nullable: true,
    unique: true,
  })
  userIdGoogle?: string;

  @ManyToOne(
    () => GroupStudents,
    groupStudents => groupStudents.students,
  )
  groupStudents?: GroupStudents;

  @OneToMany(
    () => CompletedQuestionPool,
    completedQuestionPool => completedQuestionPool.student,
  )
  completedQuestionsPool?: CompletedQuestionPool[];

  @OneToMany(
    () => CompletedPractice,
    completedPractice => completedPractice.student,
  )
  completedPractices?: CompletedPractice[];

  @OneToMany(
    () => CompletedQuestionFinalTest,
    completedQuestionFinalTest => completedQuestionFinalTest.student,
  )
  completedQuestionsFinalTest?: CompletedQuestionFinalTest[];

  @OneToOne(
    () => Application,
    application => application.student,
  )
  application?: Application;
}

export default Student;
