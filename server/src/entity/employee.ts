import {
  IsAlpha,
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsEnum,
} from 'class-validator';
import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';

import AnswerFinalTest from './answerFinalTest';
import AnswerPool from './answerPool';
import Application from './application';
import GroupStudents from './groupStudents';
import Lesson from './lesson';
import Organization from './organization';
import Practice from './practice';
import User from './user';
import QuestionFinalTest from './questionFinalTest';
import QuestionPool from './questionPool';
import Task from './task';

const MAX_LENGTH = 128;
const MIN_LENGTH = 5;

export enum EmployeeRole {
  ADMIN = 'admin',
  SUPER_ADMIN = 'super_admin',
  TEACHER = 'teacher',
}

@Entity('employees')
class Employee extends User {
  @IsAlpha()
  @Column()
  middleName!: string;

  @IsEnum(EmployeeRole)
  @Column({
    type: 'enum',
    enum: EmployeeRole,
    default: EmployeeRole.TEACHER,
  })
  role!: EmployeeRole;

  @IsNotEmpty()
  @MinLength(MIN_LENGTH)
  @MaxLength(MAX_LENGTH)
  @Column()
  password!: string;

  @IsNotEmpty()
  @ManyToOne(
    () => Organization,
    organization => organization.employees,
    {
      nullable: false,
    },
  )
  organization!: Organization;

  @ManyToOne(
    () => Employee,
    employee => employee,
  )
  creator?: Employee;

  @OneToMany(
    () => Application,
    application => application.employee,
  )
  applications?: Application[];

  @OneToMany(
    () => Employee,
    employee => employee.creator,
  )
  createdBy?: Employee[];

  @OneToMany(
    () => GroupStudents,
    groupStudents => groupStudents.employee,
  )
  groupsStudents?: GroupStudents[];

  @OneToMany(
    () => AnswerFinalTest,
    answerFinalTest => answerFinalTest.creator,
  )
  answersFinalTest?: AnswerFinalTest[];

  @OneToMany(
    () => AnswerPool,
    answerPool => answerPool.creator,
  )
  answersPool?: AnswerPool[];

  @OneToMany(
    () => Lesson,
    lesson => lesson.creator,
  )
  lessons?: Lesson[];

  @OneToMany(
    () => Practice,
    practice => practice.creator,
  )
  practices?: Practice[];

  @OneToMany(
    () => QuestionFinalTest,
    questionFinalTest => questionFinalTest.creator,
  )
  questionsFinalTest?: QuestionFinalTest[];

  @OneToMany(
    () => QuestionPool,
    questionPool => questionPool.creator,
  )
  questionsPool?: QuestionPool[];

  @OneToMany(
    () => Task,
    task => task.creator,
  )
  tasks?: Task[];
}

export default Employee;
