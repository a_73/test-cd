import { IsBoolean, IsNotEmpty } from 'class-validator';
import { Entity, ManyToOne, OneToMany, Column } from 'typeorm';

import CompletedQuestionPool from './completedQuestionPool';
import Content from './content';
import Employee from './employee';
import QuestionPool from './questionPool';

@Entity('answers_pool')
class AnswerPool extends Content {
  @IsBoolean()
  @Column()
  isTrue!: boolean;

  @IsNotEmpty()
  @ManyToOne(
    () => QuestionPool,
    questionPool => questionPool.answersPool,
    {
      nullable: false,
    },
  )
  questionPool!: QuestionPool;

  @IsNotEmpty()
  @ManyToOne(
    () => Employee,
    employee => employee.answersPool,
    {
      nullable: false,
    },
  )
  creator!: Employee;

  @OneToMany(
    () => CompletedQuestionPool,
    completedQuestionPool => completedQuestionPool,
  )
  completedQuestionsPool?: CompletedQuestionPool[];
}

export default AnswerPool;
