import { IsNotEmpty } from 'class-validator';
import { BaseEntity, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import AnswerFinalTest from './answerFinalTest';
import Lesson from './lesson';
import QuestionFinalTest from './questionFinalTest';
import Student from './student';

@Entity('completed_questions_final_test')
class CompletedQuestionFinalTest extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @IsNotEmpty()
  @ManyToOne(
    () => Student,
    student => student.completedQuestionsFinalTest,
    {
      nullable: false,
    },
  )
  student!: Student;

  @IsNotEmpty()
  @ManyToOne(
    () => Lesson,
    lesson => lesson.completedQuestionsFinalTest,
    {
      nullable: false,
    },
  )
  lesson!: Lesson;

  @IsNotEmpty()
  @ManyToOne(
    () => QuestionFinalTest,
    questionFinalTest => questionFinalTest.completedQuestionsFinalTest,
    {
      nullable: false,
    },
  )
  questionFinalTest!: QuestionFinalTest;

  @IsNotEmpty()
  @ManyToOne(
    () => AnswerFinalTest,
    answerFinalTest => answerFinalTest.completedQuestionsFinalTest,
    {
      nullable: false,
    },
  )
  answerFinalTest!: AnswerFinalTest;
}

export default CompletedQuestionFinalTest;
