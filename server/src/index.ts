import 'reflect-metadata';

import cors from '@koa/cors';
import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import helmet from 'koa-helmet';
import logger from 'koa-pino-logger';
import pino from 'pino';
import { createConnection } from 'typeorm';

import router from './routes';

const log = pino();

createConnection()
  .then(() => {
    const app = new Koa();

    app.use(helmet());
    app.use(cors());
    app.use(bodyParser());
    app.use(logger());

    app.use(router.allowedMethods());
    app.use(router.routes());

    app.listen(process.env.PORT, () => {
      log.info(
        `🚀 Server running at http://localhost:${process.env.PORT} in ${process.env.NODE_ENV} mode`,
      );
    });
  })
  .catch(error => log.error(`TypeORM connection error: ${error}`));
